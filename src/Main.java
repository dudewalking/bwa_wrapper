import java.io.IOException;
import java.net.URISyntaxException;


public class Main {

    public static void main(String[] args) throws IOException, InterruptedException, URISyntaxException {
        ProcessBuilder processBuilder = new ProcessBuilder();
        BwaWrapper bwaWrapper = new BwaWrapper();
        bwaWrapper.launchBwa(processBuilder, args);
    }
}
