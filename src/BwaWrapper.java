import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BwaWrapper {

    private Map<String, String> getPartialKeys(String[] args) {
        return Stream.of("@RG", ".sam", ".fa", ".fasta", "output_sam_")
                .collect(Collectors.toMap(Function.identity(),
                        key -> findRelativeValueForKey(key, args)));
    }

    private List<String> getMultipleKeys(String[] args) {
        return Stream.of(args)
                .filter(a -> a.contains(".fastq"))
                .collect(Collectors.toList());
    }

    private String findRelativeValueForKey(String key, String[] args) {
        return Stream.of(args)
                .filter(a -> a.endsWith(key))
                .findFirst().orElse("");
    }

    private String getFastaFile(Map<String, String> partialKeys) {
        return partialKeys.get(".fasta").isEmpty()
                ? partialKeys.get(".fa")
                : partialKeys.get(".fasta");
    }

    public void launchBwa(ProcessBuilder builder, String[] args)
            throws IOException, InterruptedException, URISyntaxException {

        Map<String, String> partialKeys = getPartialKeys(args);
        List<String> multipleKeys = getMultipleKeys(args);

        String pathToBwa = installBwa(builder);
        String fasta = getFastaFile(partialKeys);

        runBwaIndex(builder, pathToBwa, fasta);
        String script = generateScriptForBwaMem(pathToBwa, fasta, partialKeys, multipleKeys, args);
        runBwaMem(builder, script);
    }

    private String getArgs(String[] args) {
        return Stream.of(args).collect(Collectors.joining(" "));
    }

    private String generateScriptForBwaMem(String pathToBwa, String fasta, Map<String, String> partialKeys,
                                           List<String> multipleKeys, String[] args) {

        Pattern p0 = Pattern.compile("mem");
        Pattern p1 = Pattern.compile("-t \\d+");
        Pattern p2 = Pattern.compile("(-M) true");
        Pattern p3 = Pattern.compile("-R \"@RG.*[\"]");

        Matcher m0 = p0.matcher(getArgs(args));
        Matcher m1 = p1.matcher(getArgs(args));
        Matcher m2 = p2.matcher(getArgs(args));
        Matcher m3 = p3.matcher(getArgs(args));

        String beginning = m0.find() ? "bwa " + m0.group() : "";
        String threads = m1.find() ? " " + m1.group() : "";
        String mark = m2.find() ? " " + m2.group(1) : "";
        String reference = m3.find() ? " " + m3.group() : "";

        String fastqFiles = " " + fasta;

        for (String fastq : multipleKeys) {
            fastqFiles += " " + fastq;
        }
        String outputSign = " > ";

        String outputFile;

        if (partialKeys.get(".sam") != null) {
            outputFile = partialKeys.get(".sam");
        } else if (partialKeys.get("output_sam_") != null) {
            outputFile = partialKeys.get("output_sam_");
        } else {
            outputFile = "unnamed.sam";
        }

        return pathToBwa + beginning + threads + mark + reference + fastqFiles + outputSign + outputFile;
    }

    public String installBwa(ProcessBuilder builder) throws IOException, InterruptedException {
        String dir = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
        String pathToJar = dir.substring(0, dir.length() - 7);
        String pathToBwa = pathToJar.concat("bwa-0.7.15/");

        String script = "unzip -o " + dir + " 'bwa-0.7.15/*' -d " + pathToJar;
        String[] command = new String[]{"/bin/bash", "-c", script + "; chmod +xrw " + pathToBwa + "bwa"};
        Process p = builder.command(command).inheritIO().start();
        p.waitFor();
        return pathToBwa;
    }

    public void runBwaIndex(ProcessBuilder builder, String pathToBwa, String fasta)
            throws IOException, InterruptedException {
        String script = pathToBwa + "bwa index " + fasta;
        String[] command = new String[]{"/bin/bash", "-c", script};
        System.out.println("Compiled command: " + Arrays.toString(command));
        Process p = builder.command(command).inheritIO().start();
        p.waitFor();
    }

    public void runBwaMem(ProcessBuilder builder, String script)
            throws IOException, InterruptedException {
        String[] command = new String[]{"/bin/bash", "-c", script};
        System.out.println("Compiled command: " + Arrays.toString(command));
        Process p = builder.command(command).inheritIO().start();
        p.waitFor();
    }
}

